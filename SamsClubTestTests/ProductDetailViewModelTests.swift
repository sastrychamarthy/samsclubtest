import Foundation
import XCTest
@testable import SamsClubTest

class ProductDetailViewModelTests: XCTestCase {
    
    func testProductDetailViewModel() {
        do {
            let response: ProductDetail = try TestHelper.decodedObject(fromFileNamed: "ProductResponseSanitized")
            let productDetailViewModel: ProductDetailViewModel = ProductDetailViewModel(with: response)
            let productName = productDetailViewModel.productName
            let productImage = productDetailViewModel.productImageString
            let price = productDetailViewModel.productPrice
            let rating = productDetailViewModel.productRating
            let inStock = productDetailViewModel.productAvailabilityString
            let reviewLongDescription = productDetailViewModel.productRatingLongDescription
            let productId = productDetailViewModel.productId
            XCTAssert(productName == "Ellerton TV Console")
            XCTAssert(productImage == "https://mobile-tha-server.firebaseapp.com/images/image2.jpeg")
            XCTAssert(price == "$949.00")
            XCTAssert(rating == "Rating: 2.0/5")
            XCTAssert(inStock == "Ready to ship")
            XCTAssert(reviewLongDescription == "Rating: 2.0/5(1 Customer Reviews)")
            XCTAssert(productId == "003e3e6a")
        } catch {
            XCTFail()
        }
    }
    
}
