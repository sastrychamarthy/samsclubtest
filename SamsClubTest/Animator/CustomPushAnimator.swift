//
//  FadePushAnimator.swift
//  SamsClubTest
//
//  Created by Sastry Chamarthy on 10/25/18.
//  Copyright © 2018 Sastry Chamarthy. All rights reserved.
//

import Foundation
import UIKit

class CustomPushAnimator: CustomAnimator {
    
    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let toViewController = transitionContext.viewController(forKey: .to)
            else {
                return
        }
        transitionContext.containerView.addSubview(toViewController.view)
        toViewController.view.alpha = 0
        toViewController.view.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            toViewController.view.alpha = 1
            toViewController.view.transform = .identity
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
