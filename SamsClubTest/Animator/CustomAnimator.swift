//
//  CustomAnimator.swift
//  SamsClubTest
//
//  Created by Sastry Chamarthy on 10/25/18.
//  Copyright © 2018 Sastry Chamarthy. All rights reserved.
//

import Foundation
import UIKit

open class CustomAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    public enum TransitionType {
        case navigation
        case modal
    }
    
    let type: TransitionType
    let duration: TimeInterval
    
    public init(type: TransitionType, duration: TimeInterval = 0.3) {
        self.type = type
        self.duration = duration
        
        super.init()
    }
    
    open func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }
    
    open func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        fatalError("You have to implement this method for yourself!")
    }
}
