import Foundation

struct ProductDetail: Codable {
    
        let productId: String
        let productName: String
        let shortDescription: String?
        let longDescription: String?
        let price: String
        let productImage: String
        let reviewRating: Float
        let reviewCount: Int
        let inStock: Bool
}

extension String {
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}



