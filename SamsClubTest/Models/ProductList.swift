import Foundation

struct ProductList: Codable {
    
    let products: [ProductDetail]
    let totalProducts: Int
    let pageNumber: Int
    let pageSize: Int
    let statusCode: Int
}
