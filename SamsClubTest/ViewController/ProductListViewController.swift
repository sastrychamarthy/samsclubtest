import Foundation
import UIKit

class ProductListViewController: UIViewController {
    
    @IBOutlet var productTableView: LazyTableView!
    @IBOutlet var productCountLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    let productListAPI = ProductListAPI()
    var products: [ProductDetail] = []
    var currentCursor : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Products"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.productTableView.alpha = 0.0
        activityIndicator.alpha = 0.0
        productCountLabel.alpha = 0.0
        productTableView.registerDefaultNibbable(cellClass: ProductTableViewCell.self)
        productTableView.rowHeight = UITableViewAutomaticDimension
        productTableView.estimatedRowHeight = 200
        productTableView.currentCursor = currentCursor
        fetchNewItems()
        productTableView.lazyLoad.enabled = true
    }
    
    private func fetchNewItems() {
        activityIndicator.alpha = 1.0
        activityIndicator.startAnimating()
        productListAPI.getProductList(pageNumber: currentCursor, pageSize: 15, success: { [weak self] (productList) in
            guard let strongSelf = self else { return }
            
            strongSelf.currentCursor += 1
            let products = productList.products
            strongSelf.products.append(contentsOf: products)
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    strongSelf.activityIndicator.stopAnimating()
                    strongSelf.activityIndicator.alpha = 0.0
                    strongSelf.productCountLabel.alpha = 1.0
                    strongSelf.productTableView.alpha = 1.0
                    strongSelf.productTableView.reloadData()
                    strongSelf.productCountLabel.text = "\(strongSelf.products.count) products loaded, scroll down to load more products."
                })
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
            self?.handleError()
            }
        }
    }
    
}

extension ProductListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductTableViewCell = productTableView.dequeueReusableCell(withClass: ProductTableViewCell.self, reuseIdentifier: reuseIdentifier, forIndexPath: indexPath)
        let viewModel = viewModelAtIndex(index: indexPath.row)
        cell.configureWithProduct(productDetailViewModel: viewModel)
        return cell
    }
    
    private var reuseIdentifier: String {
        return ProductTableViewCell.defaultIdentifier()
    }
    
    private func viewModelAtIndex(index: Int) -> ProductDetailViewModel {
        let product = products[index]
        return ProductDetailViewModel(with: product)
    }
}

extension ProductListViewController: LazyTableViewDelegate {
    
    func tableView(tableView: UITableView, lazyLoadNextCursor cursor: Int) {
        fetchNewItems()
        productTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: .Main)
        let productDetailViewController: ProductDetailViewController = mainStoryboard.instantiateViewController()
        let viewModel = viewModelAtIndex(index: indexPath.row)
        productDetailViewController.configureWithViewModel(viewModel: viewModel)
        navigationController?.delegate = self
        navigationController?.pushViewController(productDetailViewController, animated: true)
    }
    
}

extension ProductListViewController {
    func handleError() {
        activityIndicator.alpha = 0.0
        productCountLabel.isHidden = true
        errorAlert.showFrom(self)
        
    }
    
    var errorAlert: UIAlertController {
        return UIAlertController
            .Alert("This is awkward", message: "\nLooks like something went wrong.\nPlease try again later.")
            .withOKAction(handler: { _ in})
    }
}

extension ProductListViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return CustomPushAnimator(type: .navigation)
        case .pop:
            return CustomPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
}

